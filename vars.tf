# Defining Region
variable "aws_region" {
  default = "ca-central-1"
}

# Defining CIDR Block for VPC
variable "vpc_cidr" {
  default = "10.0.0.0/16"
}


# Defining CIDR Block for VPC
variable "RSA_PUB_KEY" {
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC/gJVPt/CBrDu4aDLX56CXZ3ey6NlBDKr9yGBVESutfBGCfP+qLu5V/MTUN8rxlDLe/U5wkLfrWIQrOOrKs1/TtCFcF31d0BByukpdfjVDnjnjnPoQmyYEpJwWezxUwg8dYNj2Zh2TJ7Ai38vZqTJJhtnMb7WtJqBAZDssutYfrCuVU/2hXZNla07d+dZTX6DcLELV2OyS1UzjenzSPMSY2R1DZ1j7+5oNIVmD3wwhDmYt4vhTiCWtP0+5Ctri9sqQr/wtDqnRDyuU7DZosH+OfXTwvsQfY+L45N/Mi3mK5UFKzUJm5/v+6VWz4F3Os743+lb/GNl1UtjJPuHUOh29/RpPQz5wPRg8soG55dq0e2qkNqcdoP95G7Tf1KLl6y1WQRODeh+BrsSPvCfYkS/ZjaBHQiXUHrp7MfV8R9tgjSHwk+EnmVUsYdUO80S2gXicMKmcDpq6utLx+6gkXdMkzz/cKvyAPL/rGhYRKRd0MZDVCC2RtCSn32Cz4+2v4+mytO8g0Je9YecMF14Aooj3aYDll7TvP/BOB7aQkxui0EdOF9CUOGSC3d4UB52t2nydZc3TTwpVLTbWqwFtxcJFG+oj+V15/YH9RrA6lZoPu2H9V6cisICZH2C7AVdXymj2HMxIyZctmvPeSiLQBYnYtkAQBJUW62geP/PbwY19Cw== harshLAD@Harshkumars-MacBook-Pro.local"
}
